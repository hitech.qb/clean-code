## Generate coverage html report file
coverage:
	go test -coverprofile=coverage.out ./repo/... ./usecase/...
	go tool cover -html=coverage.out

## Generate all mock files
mock:
	mockery -all -recursive -case=underscore

## Generate all mock storage and repo files
mock-all:
	mock-product-storage mock-product-repo

## Generate product storage mock file
mock-product-storage:
	mockery -name=ProductStorage -case=underscore -dir=./storage -output=./mocks

## Generate product repo mock file
mock-product-repo:
	mockery -name=ProductRepo -case=underscore -dir=./repo -output=./mocks