module clean-code

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/logger v1.0.1
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/ugorji/go/codec v0.0.0-20190320090025-2dc34c0b8780 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/jeevatkm/go-model.v1 v1.1.0
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
