package repo

import (
	"clean-code/models"
	"clean-code/models/models_request"
	"context"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

// TEST: GetProductById
func TestProductRepo_GetProductById(t *testing.T) {
	cases := []struct {
		name           string
		requestId      int
		responseFunc   *models.Product
		errorFunc      error
		responseExpect *models.Product
		errorExpect    error
	}{
		{
			name:           "case 1: Get Error [nil - error]",
			requestId:      1,
			responseFunc:   nil,
			errorFunc:      gorm.ErrInvalidSQL,
			responseExpect: nil,
			errorExpect:    gorm.ErrInvalidSQL,
		},

		{
			name:           "case 2: Get record not found [nil - nil]",
			requestId:      1,
			responseFunc:   nil,
			errorFunc:      nil,
			responseExpect: nil,
			errorExpect:    nil,
		},

		{
			name:           "case 3: Get success [product - nil]",
			requestId:      1,
			responseFunc:   &models.Product{Id: 1, Name: "test name", Price: 10000},
			errorFunc:      nil,
			responseExpect: &models.Product{Id: 1, Name: "test name", Price: 10000},
			errorExpect:    nil,
		},
	}

	for _, v := range cases {
		t.Run(v.name, func(t *testing.T) {
			mocker := NewProductMocker()
			mocker.storage.On("GetProductById", mock.Anything, v.requestId).Once().Return(v.responseFunc, v.errorFunc)
			actual, err := mocker.repo.GetProductById(context.TODO(), v.requestId)
			assert.Equal(t, v.responseExpect, actual)
			assert.Equal(t, v.errorExpect, err)
		})
	}
}

// TEST: GetAllProduct
func TestProductRepo_GetAllProduct(t *testing.T) {
	var productsCase3 []*models.Product
	productsCase3 = append(productsCase3, &models.Product{Id: 1})

	cases := []struct {
		name           string
		responseFunc   []*models.Product
		errorFunc      error
		responseExpect []*models.Product
		errorExpect    error
	}{
		{
			name:           "case 1: Get Error [nil - error]",
			responseFunc:   nil,
			errorFunc:      gorm.ErrInvalidSQL,
			responseExpect: nil,
			errorExpect:    gorm.ErrInvalidSQL,
		},

		{
			name:           "case 2: Get record not found [nil - nil]",
			responseFunc:   nil,
			errorFunc:      nil,
			responseExpect: nil,
			errorExpect:    nil,
		},

		{
			name:           "case 3: Get success [productsCase3 - nil]",
			responseFunc:   productsCase3,
			errorFunc:      nil,
			responseExpect: productsCase3,
			errorExpect:    nil,
		},
	}

	for _, v := range cases {
		t.Run(v.name, func(t *testing.T) {
			mocker := NewProductMocker()
			mocker.storage.On("GetAllProduct", mock.Anything).Once().Return(v.responseFunc, v.errorFunc)
			actual, err := mocker.repo.GetAllProduct(context.TODO())
			assert.Equal(t, v.responseExpect, actual)
			assert.Equal(t, v.errorExpect, err)
		})
	}
}

// TEST: Create Product
func TestProductRepo_CreateProduct(t *testing.T) {
	cases := []struct {
		name           string
		request        *models_request.CreateProductRequest
		responseFunc   *models.Product
		errorFunc      error
		responseExpect *models.Product
		errorExpect    error
	}{
		{
			name:           "case 1: Insert Error [nil - error]",
			request:        &models_request.CreateProductRequest{Id: 1},
			responseFunc:   nil,
			errorFunc:      gorm.ErrInvalidSQL,
			responseExpect: nil,
			errorExpect:    gorm.ErrInvalidSQL,
		},

		{
			name:           "case 2: Insert success [product - nil]",
			request:        &models_request.CreateProductRequest{Id: 1},
			responseFunc:   &models.Product{Id: 1},
			errorFunc:      nil,
			responseExpect: &models.Product{Id: 1},
			errorExpect:    nil,
		},
	}

	for _, v := range cases {
		t.Run(v.name, func(t *testing.T) {
			mocker := NewProductMocker()
			mocker.storage.On("CreateProduct", mock.Anything, mock.Anything).Once().Return(v.responseFunc, v.errorFunc)
			actual, err := mocker.repo.CreateProduct(context.TODO(), v.request)
			assert.Equal(t, v.responseExpect, actual)
			assert.Equal(t, v.errorExpect, err)
		})
	}
}
