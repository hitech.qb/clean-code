package repo

import (
	"clean-code/models"
	"clean-code/models/models_request"
	"clean-code/storage"
	"context"
	"github.com/google/logger"
)

type ProductRepo interface {
	GetProductById(ctx context.Context, productId int) (*models.Product, error)
	GetAllProduct(ctx context.Context) ([]*models.Product, error)
	CreateProduct(ctx context.Context, product *models_request.CreateProductRequest) (*models.Product, error)
}

type productRepo struct {
	storage storage.ProductStorage
	logger  logger.Logger
}

func NewProductRepo(storage storage.ProductStorage, logger logger.Logger) *productRepo {
	return &productRepo{storage: storage, logger: logger}
}
