package repo

import (
	"clean-code/mocks"
	"github.com/google/logger"
)

type productMocker struct {
	storage *mocks.ProductStorage
	repo    ProductRepo
}

func NewProductMocker() *productMocker {
	storage := &mocks.ProductStorage{}
	appLogger := logger.Logger{}

	repo := NewProductRepo(storage, appLogger)
	return &productMocker{
		storage: storage,
		repo:    repo,
	}
}
