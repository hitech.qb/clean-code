package repo

import (
	"clean-code/models"
	"clean-code/models/models_request"
	"context"
)

func (repo *productRepo) GetProductById(ctx context.Context, productId int) (*models.Product, error) {
	return repo.storage.GetProductById(ctx, productId)
}

func (repo *productRepo) GetAllProduct(ctx context.Context) ([]*models.Product, error) {
	return repo.storage.GetAllProduct(ctx)
}

func (repo *productRepo) CreateProduct(ctx context.Context, productRequest *models_request.CreateProductRequest) (*models.Product, error) {
	return repo.storage.CreateProduct(ctx, productRequest.ToModelRequest())
}
