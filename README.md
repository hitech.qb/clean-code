# clean-code-golang
Project clean-code example (has testing) for golang

# How to run
Clone the project to your favorite folder.
Connect mysql with docker or install mysql in local.

```bash
 docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql
 ```
 
 Run docker: 
 ```bash
 docker start mysql
 ```
 
Use user & pass in .env file project. 
