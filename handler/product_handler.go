package handler

import (
	"clean-code/common"
	"clean-code/common/constant"
	"clean-code/models/models_request"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// GET: Product By Id
func (h *productHandler) GetProductById(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, constant.ErrBadRequest)
		return
	}

	product, err := h.useCase.GetProductById(ctx, id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, constant.ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, common.AppResponse{
		Code: http.StatusOK,
		Data: product,
	})
}

// GET: All Product - Not Paging
func (h *productHandler) GetAllProduct(ctx *gin.Context) {
	products, err := h.useCase.GetAllProduct(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, constant.ErrInternalServer)
		return
	}

	ctx.JSON(http.StatusOK, common.AppResponse{
		Code: http.StatusOK,
		Data: products,
	})
}

// POST: Create a Product
func (h *productHandler) CreateProduct(ctx *gin.Context) {
	var productRequest models_request.CreateProductRequest
	if err := ctx.ShouldBindJSON(&productRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, constant.ErrBadRequest)
		return
	}

	product, err := h.useCase.CreateProduct(ctx, &productRequest)
	if err != nil {
		ctx.JSON(common.CheckError(err))
		return
	}

	status := true
	ctx.JSON(http.StatusOK, common.AppResponse{
		Code:   http.StatusOK,
		Data:   product,
		Status: &status,
	})
}
