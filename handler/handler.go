package handler

import (
	"clean-code/usecase"
	"github.com/gin-gonic/gin"
)

type ProductHandler interface {
	GetProductById(ctx *gin.Context)
	GetAllProduct(ctx *gin.Context)
	CreateProduct(ctx *gin.Context)
}

type productHandler struct {
	useCase usecase.ProductUseCase
}

func NewProductHandler(useCase usecase.ProductUseCase) *productHandler {
	return &productHandler{
		useCase: useCase,
	}
}
