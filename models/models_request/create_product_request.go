package models_request

import "clean-code/models"

type CreateProductRequest struct {
	Id    int     `json:"id"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

func (p CreateProductRequest) ToModelRequest() *models.Product {
	return &models.Product{
		Id:    p.Id,
		Name:  p.Name,
		Price: p.Price,
	}
}
