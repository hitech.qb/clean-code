package models

type Product struct {
	Id    int     `gorm:"column:Id;primary_key" json:"id"`
	Name  string  `gorm:"column:Name" json:"name"`
	Price float64 `gorm:"column:Price" json:"price"`
}

func (Product) TableName() string {
	return "Product"
}
