package usecase

import (
	"clean-code/common/constant"
	"clean-code/models"
	"clean-code/models/models_request"
	"context"
	"errors"
)

func (u *productUseCase) GetProductById(ctx context.Context, productId int) (*models.Product, error) {
	return u.repo.GetProductById(ctx, productId)
}

func (u *productUseCase) GetAllProduct(ctx context.Context) ([]*models.Product, error) {
	return u.repo.GetAllProduct(ctx)
}

func (u *productUseCase) CreateProduct(ctx context.Context, productRequest *models_request.CreateProductRequest) (*models.Product, error) {
	response, _ := u.repo.GetProductById(ctx, productRequest.Id)
	if response != nil {
		return nil, errors.New(constant.ErrConflict)
	}

	return u.repo.CreateProduct(ctx, productRequest)
}
