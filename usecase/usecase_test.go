package usecase

import (
	"clean-code/mocks"
)

type productMocker struct {
	repo *mocks.ProductRepo
	uc   ProductUseCase
}

func NewProductMocker() *productMocker {
	repo := &mocks.ProductRepo{}
	uc := NewProductUseCase(repo)
	return &productMocker{
		repo: repo,
		uc:   uc,
	}
}
