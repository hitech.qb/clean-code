package usecase

import (
	"clean-code/models"
	"clean-code/models/models_request"
	"clean-code/repo"
	"context"
)

type ProductUseCase interface {
	GetProductById(ctx context.Context, productId int) (*models.Product, error)
	GetAllProduct(ctx context.Context) ([]*models.Product, error)
	CreateProduct(ctx context.Context, productRequest *models_request.CreateProductRequest) (*models.Product, error)
}

type productUseCase struct {
	repo repo.ProductRepo
}

func NewProductUseCase(repo repo.ProductRepo) *productUseCase {
	return &productUseCase{repo: repo}
}
