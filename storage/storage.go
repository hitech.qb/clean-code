package storage

import (
	"clean-code/models"
	"context"
	"github.com/google/logger"
	"github.com/jinzhu/gorm"
)

type ProductStorage interface {
	GetProductById(ctx context.Context, productId int) (*models.Product, error)
	GetAllProduct(ctx context.Context) ([]*models.Product, error)
	CreateProduct(ctx context.Context, product *models.Product) (*models.Product, error)
}

type productStorage struct {
	db     *gorm.DB
	logger logger.Logger
}

func NewProductStorage(db *gorm.DB, logger logger.Logger) *productStorage {
	return &productStorage{
		db:     db,
		logger: logger,
	}
}
