package storage

import (
	"clean-code/models"
	"context"
	"github.com/jinzhu/gorm"
)

// GET: Product by Id
func (s *productStorage) GetProductById(ctx context.Context, productId int) (*models.Product, error) {
	var product models.Product

	if err := s.db.New().First(&product, "Id=?", productId).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}

		s.logger.Errorf("Error executive sql - GetProductById ProductStorage: %v", err)
		return nil, err
	}

	return &product, nil
}

// GET: All Product
func (s *productStorage) GetAllProduct(ctx context.Context) ([]*models.Product, error) {
	var products []*models.Product
	if err := s.db.New().Find(&products).Error; err != nil {
		s.logger.Errorf("Error executive sql - GetAllProduct ProductStorage: %v", err)
		return nil, err
	}

	return products, nil
}

// POST: Create product
func (s *productStorage) CreateProduct(ctx context.Context, product *models.Product) (*models.Product, error) {
	if err := s.db.New().Create(&product).Error; err != nil {
		s.logger.Errorln(err.Error())
		return nil, err
	}

	return product, nil
}
