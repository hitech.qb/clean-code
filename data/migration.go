package data

import (
	"clean-code/common/constant"
	"clean-code/models"
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"os"
)

type Migration interface {
	MigrationData() error
	// InitProduct() error
}
type migration struct {
	db *gorm.DB
}

func NewMigration(db *gorm.DB) *migration {
	return &migration{db: db}
}

// Config migration database
func (m *migration) MigrationData() error {
	if m.db == nil {
		return errors.New("DB driver is nil")
	}

	if err := m.db.AutoMigrate(&models.Product{}).Error; err != nil {
		log.Println(constant.ErrMigration, err)
		return errors.New(constant.ErrMigration)
	}

	return m.initProduct()
}

// Init Database Product
func (m *migration) initProduct() error {
	jsonData, err := os.Open("data/product.json")
	if err != nil {
		log.Println(constant.ErrOpenJsonFile, err)
		return errors.New(constant.ErrOpenJsonFile)
	}

	defer jsonData.Close()
	data, err := ioutil.ReadAll(jsonData)
	if err != nil {
		log.Println(constant.ErrReadJsonFile, err)
		return errors.New(constant.ErrReadJsonFile)
	}

	var products []models.Product
	err = json.Unmarshal(data, &products)
	if err != nil {
		log.Println(constant.ErrUnMarshal, " - PRODUCT: ", err)
		return errors.New(constant.ErrUnMarshal + " - PRODUCT")
	}

	for _, product := range products {
		m.db.Create(&product)
	}

	println("Seeded Product Database")
	return nil
}
