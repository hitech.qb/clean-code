package main

import (
	"clean-code/data"
	"clean-code/handler"
	"clean-code/repo"
	"clean-code/storage"
	"clean-code/usecase"
	"github.com/gin-gonic/gin"
	"github.com/google/logger"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	"os"

	//_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)

// Load Environment
func LoadEnvironment() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

// Setup run app
func RunApp() {
	// Load .env
	LoadEnvironment()

	db, err := gorm.Open(os.Getenv("GORM_DB_TYPE"), os.Getenv("GORM_DB_URI"))
	if err != nil {
		logger.Errorln("Error Connect DB: %v", err.Error())
		panic(err)
	}

	defer db.Close()
	db.LogMode(true) // LogMode query to database

	// Config Migration
	mr := data.NewMigration(db)
	if err := mr.MigrationData(); err != nil {
		logger.Errorln("Error Migration: %v", err.Error())
		panic(err)
	}

	// Config app router
	router := gin.Default()
	Router(router, db)

	// Run App
	log.Fatal(router.Run(":8888"))
}

// Init router
func Router(router *gin.Engine, db *gorm.DB) {
	appLog := logger.Logger{}
	storageProduct := storage.NewProductStorage(db, appLog)
	repoProduct := repo.NewProductRepo(storageProduct, appLog)
	useCaseProduct := usecase.NewProductUseCase(repoProduct)
	handlerProduct := handler.NewProductHandler(useCaseProduct)
	v1 := router.Group("/v1")

	// Product
	v1.GET("/product/:id", handlerProduct.GetProductById)
	v1.GET("/products", handlerProduct.GetAllProduct)
	v1.POST("/product/", handlerProduct.CreateProduct)
}
