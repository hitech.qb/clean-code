package common

type AppResponse struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data,omitempty"`
	Status  *bool       `json:"status,omitempty"`
	Message *string     `json:"message,omitempty"`
	Other   interface{} `json:"other,omitempty"`
}
