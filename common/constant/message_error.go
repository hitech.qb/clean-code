package constant

const ErrBadRequest = "data is invalid"
const ErrConflict = "data is conflict"
const ErrInternalServer = "cannot product this function"

const ErrMigration = "error while migration database"
const ErrOpenJsonFile = "error while open json file"
const ErrReadJsonFile = "error while read json file"
const ErrUnMarshal = "error while unmarshal json file"
