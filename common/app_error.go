package common

import (
	"clean-code/common/constant"
	"net/http"
)

func CheckError(err error) (code int, message string) {
	if isConflict(err) {
		return http.StatusConflict, constant.ErrConflict
	}

	// default: Code 50 - Internal Server
	return http.StatusInternalServerError, constant.ErrInternalServer
}

// Conflict Error - 409
func isConflict(err error) bool {
	return err.Error() == constant.ErrConflict
}
